package coop.tecso.examen.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import coop.tecso.examen.dao.TitularDAO;
import coop.tecso.examen.model.Titular;

@Repository
public class TitularDAOImpl implements TitularDAO{
	
	private static DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		TitularDAOImpl.dataSource = dataSource;
	}
	
	public void crearTitularFisico(Titular t) throws SQLException {
		
		ResultSet rs = null;
		PreparedStatement pst = null;
		Connection conn = null;
				
		try {
			conn = dataSource.getConnection();
			StringBuilder sb = new StringBuilder("insert into titular(tipoPersona, nombre, apellido, dni, cuit)") ;
			sb.append(" values(?,?,?,?,?)");
			pst = conn.prepareStatement(sb.toString());
			
			int i = 0;
			pst.setString(i++, t.getTipoPersona());
			pst.setString(i++, t.getNombre());
			pst.setString(i++, t.getApellido());
			pst.setString(i++, t.getDni());
			pst.setString(i++, t.getCuit());
			
			rs = pst.executeQuery(sb.toString());
			
		}catch(Exception e){
			LogFactory.getLog("trazas-log").debug("Error en guardar datos titular físico"+ rs);
		}finally {
			conn.close();
		}
		
	}
}
