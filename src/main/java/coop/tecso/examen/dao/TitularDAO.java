package coop.tecso.examen.dao;

import java.sql.SQLException;
import coop.tecso.examen.model.Titular;

public interface TitularDAO {
	public void crearTitularFisico(Titular t) throws SQLException;
}
