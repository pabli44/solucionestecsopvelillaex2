package coop.tecso.examen.dto;

import java.io.Serializable;

public class TitularDto implements Serializable{

	private static final long serialVersionUID = -6746868885752111724L;
	
	private Long id;
	private String tipoPersona;
	private String nombre;
	private String apellido;
	private String razonSocial;
	private String anoFundacion;
	private String dni;
	private String cuit;
	
	public TitularDto() {
		
	}
	
	public TitularDto(Long id, String tipoPersona, String nombre, String apellido, String dni, String cuit) {
		this.id = id;
		this.tipoPersona = tipoPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.cuit = cuit;
	}
	
	public TitularDto(Long id, String tipoPersona, String razonSocial, String anoFundacion, String cuit) {
		this.id = id;
		this.tipoPersona = tipoPersona;
		this.razonSocial = razonSocial;
		this.anoFundacion = anoFundacion;
		this.cuit = cuit;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getAnoFundacion() {
		return anoFundacion;
	}
	public void setAnoFundacion(String anoFundacion) {
		this.anoFundacion = anoFundacion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	
}
