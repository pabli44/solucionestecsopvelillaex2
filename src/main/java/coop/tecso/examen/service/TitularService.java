package coop.tecso.examen.service;

import java.sql.SQLException;
import coop.tecso.examen.model.Titular;

public interface TitularService {
	
	public void crearTitularFisico(Titular t) throws SQLException;
	
//	public void crearTitularJuridico(String tipoPersona, String razonSocial, String anoFundacion, String cuit) throws SQLException;
//	
//	public Titular consultarTitular(Long id) throws SQLException;
//
//	public void actualizarTitular(Titular t) throws SQLException;
//	
//	public void eliminarTitular(String id) throws SQLException;
	
}
