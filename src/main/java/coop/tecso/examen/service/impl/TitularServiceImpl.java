package coop.tecso.examen.service.impl;

import java.sql.SQLException;

import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.dao.TitularDAO;
import coop.tecso.examen.model.Titular;
import coop.tecso.examen.service.TitularService;

@Service
public class TitularServiceImpl implements TitularService {
	
	@Autowired
	private static TitularDAO dao;

	@Override
	public void crearTitularFisico(Titular t) throws SQLException {
		
		try {
			dao.crearTitularFisico(t);
		} catch (Exception e) {
			LogFactory.getLog("trazas-log").debug("Error en guardar datos titular físico"+ e);
		}
		
	}
	
//	@Override
//	public void crearTitularJuridico(String tipoPersona, String razonSocial, String anoFundacion, String cuit) throws SQLException {
//		Titular t = new Titular(tipoPersona, razonSocial, anoFundacion, cuit);
//		ResultSet rs = null;
//		PreparedStatement pst = null;
//		Connection conn = null;
//				
//		try {
//			StringBuilder sb = new StringBuilder("insert into titular(tipoPersona, razonSocial, anoFundacion, cuit)") ;
//			sb.append(" values(?,?,?,?)");
//			pst = conn.prepareStatement(sb.toString());
//			
//			int i = 0;
//			pst.setString(i++, t.getTipoPersona());
//			pst.setString(i++, t.getRazonSocial());
//			pst.setString(i++, t.getAnoFundacion());
//			pst.setString(i++, t.getCuit());
//			
//			rs = pst.executeQuery(sb.toString());
//			
//		}catch(Exception e){
//			LogFactory.getLog("trazas-log").debug("Error en guardar datos titular jurídico"+ rs);
//		}finally {
//			conn.close();
//		}
//		
//	}
//	
//	@Override
//	public Titular consultarTitular(Long id) throws SQLException {
//		Titular t = null;
//		ResultSet rs = null;
//		PreparedStatement pst = null;
//		Connection conn = null;
//		
//		try {
//			StringBuilder sb = new StringBuilder("select tipopersona, nombre, apellido, razonsocial, anofundacion, dni, cuit from titular where id = ?");
//			pst.setLong(0, id);
//			rs = pst.executeQuery(sb.toString());
//			
//			if(rs.next()) {
//				String tipoPersona = rs.getString("tipopersona");
//				String nombre = rs.getString("nombre");
//				String apellido = rs.getString("apellido");
//				String razonSocial = rs.getString("razonsocial");
//				String anoFundacion = rs.getString("anofundacion");
//				String dni = rs.getString("dni");
//				String cuit = rs.getString("cuit");
//				
//				if(tipoPersona.equals("fisica")) {
//					t = new Titular(tipoPersona, nombre, apellido, dni, cuit);
//				}
//				
//				if(tipoPersona.equals("juridica")) {
//					t = new Titular(tipoPersona, razonSocial, anoFundacion, cuit);
//				}
//				
//				return t;
//			}
//			
//		}catch(Exception e) {
//			LogFactory.getLog("trazas-log").debug("Error en consultar datos"+ rs);
//		}finally {
//			conn.close();
//		}
//		
//		return null;
//	}
//	
//	@Override
//	public void actualizarTitular(Titular t) throws SQLException {
//		ResultSet rs = null;
//		PreparedStatement pst = null;
//		Connection conn = null;
//		
//		try {
//			StringBuilder sb = new StringBuilder("update titular set tipopersona=?, nombre=?, apellido=?, razonsocial=?, anofundacion=?, cuit=?"); 
//			sb.append(" where dni = ?");
//			
//			int i = 0;
//			pst.setString(i++, t.getTipoPersona());
//			pst.setString(i++, t.getNombre());
//			pst.setString(i++, t.getApellido());
//			pst.setString(i++, t.getRazonSocial());
//			pst.setString(i++, t.getAnoFundacion());
//			pst.setString(i++, t.getCuit());
//			pst.setString(i++, t.getDni());
//			
//		}catch(Exception e) {
//			LogFactory.getLog("trazas-log").debug("Error en actualizarTitular"+ rs);
//		}finally {
//			conn.close();
//		}
//		
//	}
//	
//	@Override
//	public void eliminarTitular(String dni) throws SQLException {
//		ResultSet rs = null;
//		PreparedStatement pst = null;
//		Connection conn = null;
//		
//		try{
//			StringBuilder sb = new StringBuilder("delete titular where id = ?");
//			pst.setString(0, dni);
//			rs = pst.executeQuery(sb.toString());
//			
//		}catch (Exception e) {
//			LogFactory.getLog("trazas-log").debug("Error en eliminarTitular"+ rs);
//		}finally {
//			conn.close();
//		}
//		
//		
//	}
	
}
