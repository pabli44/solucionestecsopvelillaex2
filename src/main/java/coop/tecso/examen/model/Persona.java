package coop.tecso.examen.model;

import javax.persistence.Entity;

@Entity
public class Persona extends AbstractPersistentObject{

	private static final long serialVersionUID = -6769141159177314992L;
	
	private String tipoTitular;
	private String nombre;
	private String apellido;
	private String razonSocial;
	private String anoFundacion;
	private String dni;
	private String cuit;
	
	public Persona(String nombre, String apellido, String dni, String cuit) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.cuit = cuit;
	}
	
	public Persona(String razonSocial, String anoFundacion, String cuit) {
		this.razonSocial = razonSocial;
		this.anoFundacion = anoFundacion;
		this.cuit = cuit;
	}
	
	public String getTipoTitular() {
		return tipoTitular;
	}
	public void setTipoTitular(String tipoTitular) {
		this.tipoTitular = tipoTitular;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getAnoFundacion() {
		return anoFundacion;
	}
	public void setAnoFundacion(String anoFundacion) {
		this.anoFundacion = anoFundacion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

}
