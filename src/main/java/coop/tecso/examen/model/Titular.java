package coop.tecso.examen.model;

import javax.persistence.Entity;

@Entity
public class Titular extends AbstractPersistentObject{

	private static final long serialVersionUID = -6769141159177314992L;
	
	private String tipoPersona;
	private String nombre;
	private String apellido;
	private String razonSocial;
	private String anoFundacion;
	private String dni;
	private String cuit;
	
	public Titular() {
		
	}
	
	/**
	 * Constructor para tipo de persona física
	 * @param tipoPersona
	 * @param nombre
	 * @param apellido
	 * @param dni
	 * @param cuit
	 */
	public Titular(String tipoPersona, String nombre, String apellido, String dni, String cuit) {
		this.tipoPersona = tipoPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.cuit = cuit;
	}
	
	/**
	 * Constructor para tipo de persona jurídica
	 * @param tipoPersona
	 * @param razonSocial
	 * @param anoFundacion
	 * @param cuit
	 */
	public Titular(String tipoPersona, String razonSocial, String anoFundacion, String cuit) {
		this.tipoPersona = tipoPersona;
		this.razonSocial = razonSocial;
		this.anoFundacion = anoFundacion;
		this.cuit = cuit;
	}
	
	public String getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getAnoFundacion() {
		return anoFundacion;
	}
	public void setAnoFundacion(String anoFundacion) {
		this.anoFundacion = anoFundacion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

}
