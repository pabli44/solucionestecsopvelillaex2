package coop.tecso.examen.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.TitularDto;
import coop.tecso.examen.model.Titular;
import coop.tecso.examen.repository.TitularRepository;
import coop.tecso.examen.service.TitularService;

@RestController
@RequestMapping(path = "/titular")
public class TitularController {
	
	private List<TitularDto> result = null;
	
	@Autowired
	private TitularService titularService;
	
	@Autowired
	private TitularRepository titularRepository;
	
	@GetMapping("/creartitularfisico")
	public void crearTitularFisico(@RequestParam String tipoPersona, @RequestParam String nombre, @RequestParam String apellido, @RequestParam String dni, 
			@RequestParam String cuit) throws SQLException{
		
		Titular t = new Titular(tipoPersona, nombre, apellido, dni, cuit);
		titularService.crearTitularFisico(t);
	}
	
	
	/*INICIO FUNCIONES PARA H2*/
	
	@GetMapping("/createF")
	public void create(@RequestParam String tipoPersona, @RequestParam String nombre, @RequestParam String apellido, 
						@RequestParam String dni, @RequestParam String cuit) throws Exception {
		
		Titular dto = new Titular(tipoPersona, nombre, apellido, dni, cuit);
		titularRepository.save(dto);
		
	}
	
	@GetMapping("/createJ")
	public void create(@RequestParam String tipoPersona, @RequestParam String razonSocial, 
			@RequestParam String anoFundacion, @RequestParam String cuit) throws Exception {
		
		Titular dto = new Titular(tipoPersona, razonSocial, anoFundacion, cuit);
		titularRepository.save(dto);
			
	}
	
	//findAll Titulares
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/findAll")
	public List<TitularDto> findAll() {
		result = new ArrayList<>();
		for (Titular entity : titularRepository.findAll()) {
			TitularDto dto = new TitularDto();
			dto.setId(entity.getId());
			dto.setTipoPersona(entity.getTipoPersona());
			dto.setNombre(entity.getNombre());
			dto.setApellido(entity.getApellido());
			dto.setRazonSocial(entity.getRazonSocial());
			dto.setAnoFundacion(entity.getAnoFundacion());
			dto.setDni(entity.getDni());
			dto.setCuit(entity.getCuit());
			
			result.add(dto);
		}
		
	    return result;
	}
	
	//findById Titulares
	@GetMapping("/findById")
	public TitularDto findById(@RequestParam Long id) {
		
		Titular tit = titularRepository.findById(id).get();
		TitularDto dto = new TitularDto();
		dto.setId(tit.getId());
		dto.setTipoPersona(tit.getTipoPersona());
		dto.setNombre(tit.getNombre());
		dto.setApellido(tit.getApellido());
		dto.setRazonSocial(tit.getRazonSocial());
		dto.setAnoFundacion(tit.getAnoFundacion());
		dto.setDni(tit.getDni());
		dto.setCuit(tit.getCuit());

	    return dto;
	}
	
	//deleteById Titulares
	@GetMapping("/deleteById")
	public void deleteById(@RequestParam Long id) {
		
		titularRepository.deleteById(id);
		
	}
	
	//updateById Titulares
	@GetMapping("/updateById")
	public void updateById(@RequestParam Long id, @RequestParam String tipoPersona, @RequestParam String nombre, @RequestParam String apellido, @RequestParam String razonSocial,
			@RequestParam String anoFundacion, @RequestParam String dni, @RequestParam String cuit) {
		
		Titular tit = titularRepository.getOne(id);
		if(!tipoPersona.isEmpty()) {
			tit.setTipoPersona(tipoPersona);
		}
		
		if(!nombre.isEmpty()) {
			tit.setNombre(nombre);
		}
		
		if(!apellido.isEmpty()) {
			tit.setApellido(apellido);
		}
		
		if(!razonSocial.isEmpty()) {
			tit.setRazonSocial(razonSocial);
		}
		
		if(!anoFundacion.isEmpty()) {
			tit.setAnoFundacion(anoFundacion);
		}
		
		if(!dni.isEmpty()) {
			tit.setDni(dni);
		}
		
		if(!cuit.isEmpty()) {
			tit.setCuit(cuit);
		}
		
		titularRepository.save(tit);
		
	}
	
	/*FIN FUNCIONE PARA H2*/
	
	
//	@GetMapping("/creartitularjuridico")
//	public void crearTitularJuridico(@RequestParam String tipoPersona, @RequestParam String razonSocial, @RequestParam String anoFundacion, 
//			@RequestParam String cuit) throws SQLException {
//		titularService.crearTitularJuridico(tipoPersona, razonSocial, anoFundacion, cuit);
//	}
//	
//	@GetMapping("/consultartitular")
//	public Titular consultarTitular(@RequestParam Long id) throws SQLException {
//		return titularService.consultarTitular(id);
//	}
//	
//	@GetMapping("/actualizartitular")
//	public void actualizarTitular(@RequestParam Titular t) throws SQLException {
//		titularService.actualizarTitular(t);
//	}
//	
//	@GetMapping("/eliminartitular")
//	public void eliminarTitular(@RequestParam String dni) throws SQLException {
//		titularService.eliminarTitular(dni);
//	}

}
