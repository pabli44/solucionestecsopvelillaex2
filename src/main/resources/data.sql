INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('AR','ARGENTINA', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('BR','BRASIL', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('UY','URUGUAY', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CH','CHILE', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);



--TITULARES POR H2

--ALTER TABLE titular ADD PRIMARY KEY (id, cuit);
--ALTER TABLE titular ADD UNIQUE (cuit);

--fisica
INSERT INTO titular (tipo_persona, nombre, apellido, dni, cuit, creation_timestamp, modification_timestamp, version_number) 
VALUES ('F','PABLO', 'VELILLA', '12345678910', 'ZYT1128', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);


--juridica
INSERT INTO titular (tipo_persona, razon_social, ano_fundacion, cuit, creation_timestamp, modification_timestamp, version_number) 
VALUES ('J', 'pvelilla inc', '2000', 'ZYT1129', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
