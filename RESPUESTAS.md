## URL'S REPOSITORIOS
backend  -----> https://gitlab.com/pabli44/solucionestecsopvelillaex2.git 
<br>
frontend -----> https://gitlab.com/pabli44/solucionestecsopvelillaex2frontend.git




## Probando controllers:
	
Obtener todos los titulares:
<br>
curl -X GET "http://localhost:8080/tecso/titular/findAll"
	
	
Obtener titular por id:
<br>
curl -X GET "http://localhost:8080/tecso/titular/findById?id=1"


Eliminar por id:
<br>
curl -X GET "http://localhost:8080/tecso/titular/deleteById?id=1"

Crear Titular tipo Físico:
<br>
curl -X GET "http://localhost:8080/tecso/titular/createF?tipoPersona=F&nombre=Alicia&apellido=Goenaga&dni=154545488&cuit=AAJ778"

Crear Titular tipo Jurídico:
<br>
curl -X GET "http://localhost:8080/tecso/titular/createJ?tipoPersona=J&razonSocial=Velillas%20Hamburguers&anoFundacion=2003&cuit=AAJ79544"

Actualizar Titular:
<br>
curl -X GET "http://localhost:8080/tecso/titular/updateById?id=1&tipoPersona=&nombre=Alberto&apellido=Matera&razonSocial=Hamburguers Zone&anoFundacion=2007&dni=200544784&cuit=SSD909087"
